﻿using UnityEngine;
using System.Collections;

namespace Puzzalo
{
	public class RectGrid : AGrid
	{

		public RectGrid()
		{
			deltas = new int[8, 2]{ {-1, -1}, {0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0} };
		}

		#region implemented abstract members of AGrid
		public override System.Collections.Generic.List<GridItem> GetConnectedItems (int posX, int posY, int typeID)
		{
			selectedItems.Clear ();

			GetItemAt (posX, posY).isChecked = true;

			int[,] targetDeltas = new int[4, 2]{ {0, -1}, {1, 0}, {0, 1}, {-1, 0} };
			RecurrentConnectedColorSearch (posX, posY, typeID, targetDeltas);

			foreach (var item in items)
				item.isChecked = false;

			return selectedItems;
		}

		public override System.Collections.Generic.List<GridItem> GetItemsInRadius (int posX, int posY, int radius)
		{
			selectedItems.Clear ();

			var lineLength = (radius << 1) + 1;
			var startX = posX - radius;
			var step = 0;

			int tX;
			int tY;

			for (int i = 0; i < lineLength; i++)
			{
				step += ( i < lineLength / 2) ? 1 : -1;

				for (int j = (-step + 1); j < step; j++)
				{
					tX = startX + i;
					tY = posY + j;

					if (tX == posX && tY == posY)
						continue;

					var item = GetItemAt( tX, tY);

					if (item == null)
						continue;
					if (item.includedInSearch == false)
						continue;

					selectedItems.Add(item);
				}
			}

			return selectedItems;
		}
		public override System.Collections.Generic.List<GridItem> GetItemsNeighbours (int posX, int posY)
		{
			selectedItems.Clear ();

			for (int i = 0; i < deltas.GetLength(0); i++)
			{
				var tX = posX + deltas[i, 0];
				var tY = posY + deltas[i, 1];
				
				var b = GetItemAt( tX, tY );
				
				if ( b == null )
					continue;

				if ( b.includedInSearch == false)
					continue;
				
				selectedItems.Add( b );
			}

			return selectedItems;
		}
		public override Vector2 GetItemOffset (int posX, int posY)
		{
			return new Vector2( posX, posY );
		}
		#endregion

		System.Collections.Generic.List<GridItem> GetCustomNeighbours (int posX, int posY, int[,] customDeltas)
		{
			System.Collections.Generic.List<GridItem> list = new System.Collections.Generic.List<GridItem> ();
			
			for (int i = 0; i < customDeltas.GetLength(0); i++)
			{
				var tX = posX + customDeltas[i, 0];
				var tY = posY + customDeltas[i, 1];
				
				var b = GetItemAt( tX, tY );
				
				if ( b == null )
					continue;

				list.Add( b );
			}
			
			return list;
		}

		void RecurrentConnectedColorSearch (int posX, int posY, int typeID, int[,] searchDeltas)
		{
			var nb = GetCustomNeighbours (posX, posY, searchDeltas);

			foreach (var item in nb)
			{
				if (item.includedInSearch == false)
					continue;

				item.isChecked = true;

				if (item.type == typeID)
				{
					selectedItems.Add( item );
					RecurrentConnectedColorSearch( item.posX, item.posY, typeID, searchDeltas);
				}
			}
		}
	}
}