﻿using UnityEngine;
using System.Collections;

namespace Puzzalo
{
	public class GridItem : MonoBehaviour
	{
		public int id = 0;
		
		public int posX = 0;
		public int posY = 0;

		private int __type = 0;
		public int type
		{
			get {return __type;}
			set {__type = value; SetType(__type);}
		}

		public bool includedInSearch
		{   
			get
			{
				if ( isDestroyed == true || isChecked == true || isRegistered == false)
					return false;
				return true;
			}
		}
		
		public bool isDestroyed = false;
		public bool isConnected = true;
		public bool isRegistered = true;

		public bool isChecked = false;

		public AGrid grid;
		public Field field;
		public GameObject itemBody;

		void OnDestroy()
		{
			if (includedInSearch == false)
				return;
			if (grid == null)
				return;
			grid.UnregisterItem (this);
		}

		public void Reset()
		{
			isDestroyed = false;
			isConnected = true;
			isChecked = false;
			posX = 0;
			posY = 0;
		}

		private void SetType(int newTypeID)
		{
			DestroyBody ();

			itemBody = field.GetItemPrefab (newTypeID );
			itemBody.transform.parent = gameObject.transform;
			itemBody.transform.position = gameObject.transform.position;
		}

		public void DestroyBody(AnimationClip clip = null)
		{
			if (itemBody == null)
				return;
			GameObject.Destroy (itemBody);
		}

	}
}