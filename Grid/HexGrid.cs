﻿using UnityEngine;
using System.Collections;

namespace Puzzalo
{
	public class HexGrid : AGrid
	{
		private int[,] oddDeltas = new int[6,2]{{0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, { -1, 0}};
		private int[,] evenDeltas = new int[6,2]{{-1, -1}, {0, -1}, {1, 0}, {0, 1}, {-1, 1}, {-1, 0}};

		public HexGrid()
		{

		}

		#region implemented abstract members of AGrid
		public override System.Collections.Generic.List<GridItem> GetConnectedItems (int posX, int posY, int typeID = -1)
		{
			selectedItems.Clear ();

			GetItemAt (posX, posY).isChecked = true;
			RecurrentConnectedColorSearch (posX, posY, typeID);

			foreach (var item in items)
				item.isChecked = false;

			return selectedItems;
		}

		public override System.Collections.Generic.List<GridItem> GetItemsInRadius (int posX, int posY, int radius)
		{
			selectedItems.Clear ();

			bool startPosIsEven = IsEven (posY);

			for (int i = -radius; i <= radius; i++)
			{
				var absMod = (i > 0) ? i / 2 : -i / 2;

				int startNumCells = -radius + absMod;
				int endNumCells = radius - absMod;

				if (startPosIsEven == false)
					if (IsEven( i ) == false)
					startNumCells++;

				if ( startPosIsEven == true)
					if (IsEven( i ) == false)
					endNumCells--;

				for (int j = startNumCells; j <= endNumCells; j++)
				{
					int tX = posX + j;
					int tY = posY + i;

					var b = GetItemAt( tX, tY);

					if ( b == null )
						continue;
					
					if ( b.includedInSearch == false)
						continue;

					if (b.posX == posX && b.posY == posY)
						continue;
					
					selectedItems.Add( b );
				}
			}  

			return selectedItems;
		}
		public override System.Collections.Generic.List<GridItem> GetItemsNeighbours (int posX, int posY)
		{
			deltas = (IsEven (posY) == true) ? evenDeltas : oddDeltas;

			selectedItems.Clear ();
			
			for (int i = 0; i < deltas.GetLength(0); i++)
			{
				var tX = posX + deltas[i, 0];
				var tY = posY + deltas[i, 1];
				
				var b = GetItemAt( tX, tY );
				
				if ( b == null )
					continue;
				
				if ( b.includedInSearch == false)
					continue;
				
				selectedItems.Add( b );
			}
			
			return selectedItems;
		}
		public override Vector2 GetItemOffset (int posX, int posY)
		{
			float newX = (posY % 2 == 0) ? posX : posX + .5f;
			float newY = posY * .85f;
			return new Vector2(newX, newY);
		}
		#endregion

		private bool IsEven(float val)
		{
			if (val == 0) return true;
			float frac = val / 2;
			return ((int)frac == frac) ? true : false;
		}

		System.Collections.Generic.List<GridItem> GetCustomNeighbours (int posX, int posY)
		{
			System.Collections.Generic.List<GridItem> list = new System.Collections.Generic.List<GridItem> ();

			deltas = (IsEven (posY) == true) ? evenDeltas : oddDeltas;

			for (int i = 0; i < deltas.GetLength(0); i++)
			{
				var tX = posX + deltas[i, 0];
				var tY = posY + deltas[i, 1];
				
				var b = GetItemAt( tX, tY );
				
				if ( b == null )
					continue;
				
				list.Add( b );
			}
			
			return list;
		}

		void RecurrentConnectedColorSearch (int posX, int posY, int typeID)
		{
			var nb = GetCustomNeighbours (posX, posY);
			
			foreach (var item in nb)
			{
				if (item.includedInSearch == false)
					continue;
				
				item.isChecked = true;
				
				if (item.type == typeID || typeID == -1)
				{
					selectedItems.Add( item );
					RecurrentConnectedColorSearch(item.posX, item.posY, typeID);
				}
			}
		}

	}
}