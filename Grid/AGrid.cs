﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Puzzalo
{
	public abstract class AGrid
	{
		protected int[,] deltas;

		protected List<GridItem> items = new List<GridItem>();
		protected List<GridItem> selectedItems = new List<GridItem> ();

		protected int currentMaxX = 0;
		protected int currentMaxY = 0;

		public void RegisterItem(GridItem item)
		{
			items.Add (item);

			if (item.posX > currentMaxX)
				currentMaxX = item.posX;

			if (item.posY > currentMaxY)
				currentMaxY = item.posY;
		}

		public List<GridItem> GetAllItems()
		{
			return items;
		}

		public void UnregisterItem( GridItem item )
		{
			item.isRegistered = false;
			items.Remove (item);
		}

		public void MoveItems( int dirX, int dirY )
		{
			foreach (var item in items)
			{
				if (item.includedInSearch == false)
					continue;

				item.posX += dirX;
				item.posY += dirY;
			}
		}

		public List<GridItem> GetDisconnectedItems ()
		{
			selectedItems.Clear ();

			foreach (var item in items)
				if (item.isConnected == false && item.includedInSearch == true)
					selectedItems.Add(item);
			return selectedItems;	
		}

		public List<GridItem> GetItemsByPosY (int posY)
		{
			selectedItems.Clear ();
			
			foreach (var item in items)
				if (item.posY == posY && item.includedInSearch == true)
					selectedItems.Add(item);
			return selectedItems;
		}

		public object GetItemsByPosX (int posX)
		{
			selectedItems.Clear ();
			
			foreach (var item in items)
				if (item.posX == posX && item.includedInSearch == true)
					selectedItems.Add(item);
			return selectedItems;
		}

		public GridItem GetItemAt (int posX, int posY)
		{
			foreach (var item in items)
				if (item.posX == posX && item.posY == posY && item.includedInSearch == true)
					return item;
			return null;
		}

		public List<GridItem> GetItemsOfType (int typeID)
		{
			selectedItems.Clear ();

			foreach (var item in items)
				if (item.type == typeID && item.includedInSearch == true)
					selectedItems.Add(item);

			return selectedItems;
		}

		public List<GridItem> GetItemsInDirection (int startX, int startY, int dirX, int dirY)
		{
			selectedItems.Clear ();

			int offsetX = startX + dirX;
			int offsetY = startY + dirY;

			int totalCount = items.Count;
			for (int i = 0; i < totalCount; i++)
			{
				var item = GetItemAt( offsetX, offsetY);
				if (item == null)
					continue;

				if (item.includedInSearch == false)
					continue;

				selectedItems.Add(item);

				offsetX += dirX;
				offsetY += dirY;
			}

			return selectedItems;
		}

		public List<GridItem> GetItemsInDirectionByType (int startX, int startY, int dirX, int dirY, int type, bool stopOnMistype)
		{
			selectedItems.Clear ();

			int offsetX = startX + dirX;
			int offsetY = startY + dirY;

			int totalCount = items.Count;
			for (int i = 0; i < totalCount; i++)
			{
				var item = GetItemAt( offsetX, offsetY);
				if (item == null)
					continue;

				if (item.includedInSearch == false)
					continue;

				if (item.type == type)
					selectedItems.Add (item);
				else if (stopOnMistype == true)
					break;

				offsetX += dirX;
				offsetY += dirY;
			}

			return selectedItems;
		}

		public List<GridItem> GetItemsInArea (int aX, int aY, int bX, int bY)
		{
			selectedItems.Clear ();

			for (int i = aX; i <= bX; i++)
				for (int j = aY; j <= bY; j++)
				{
					var item = GetItemAt (i, j);
					if (item == null)
						continue;
					if (item.includedInSearch == false)
						continue;
					selectedItems.Add (item);
				}

			return selectedItems;
		}

		#region ABSTRACT METHODS
		public abstract List<GridItem> GetConnectedItems (int posX, int posY, int typeID = -1);
		public abstract List<GridItem> GetItemsInRadius (int posX, int posY, int radius);
		public abstract List<GridItem> GetItemsNeighbours (int posX, int posY);
		public abstract Vector2 GetItemOffset (int posX, int posY);
		#endregion
	}
}