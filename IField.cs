using System;
using UnityEngine;
using System.Collections.Generic;

namespace Puzzalo
{
	internal interface IField
	{
		#region FIELD_SETTINGS
		void SetOrigin( Vector3 origin);
		void SetOrigin( Transform origin);
		bool AddItemPrefab( GameObject itemPrefab );
		#endregion

		#region GRID_OPEARATIONS
		GameObject CreateItem( int posX, int posY, int type = -1);
		GameObject CreateItem( int posX, int posY, Vector3 position, int type = -1);

		void SetItemType(int type, GameObject item, AnimationClip changeAnimation = null);
		void SetItemType(int type, int itemPosX, int itemPosY, AnimationClip changeAnimation = null);
		void SetItemType(int type, GridItem item, AnimationClip changeAnimation = null);

		List<GameObject> FillGrid( int numX, int numY );
		List<GameObject> FillGrid( int numX, int numY, int typeID );
		List<GameObject> FillGrid( int numX, int numY, int[] typeIDs );
		List<GameObject> AddLine( int num, int lineWidth );

		void RemoveDisconnectedItems(AnimationClip clip = null);
		void DestroyItems(List<GameObject> items, AnimationClip clip = null);
		void RemoveLine( int posY, AnimationClip clip = null );
		#endregion

		#region GRID_MATCHING
		GameObject GetItemAt( int posX, int posY );

		List<GameObject> GetAllItems();
		List<GameObject> GetAllItemsOfType( int typeID );

		List<GameObject> GetItemsInRad( int posX, int posY, int radius, bool includeStartPos = true );

		List<GameObject> GetItemsInDirection( int startX, int startY, int dirX, int dirY, bool includeStartPos = true);
		List<GameObject> GetItemsInDirectionByType( int startX, int startY, int dirX, int dirY, int type, bool includeStartPos = true, bool stopOnMistype = false);

		List<GameObject> GetItemsInArea( int aX, int aY, int bX, int bY);

		List<GameObject> GetItemsNeighbours( int posX, int posY );
		List<GameObject> GetItemsNeighbours( GameObject targetItem );
		List<GameObject> GetItemsNeighbours( GridItem targetItem );

		List<GameObject> GetConnectedItems( int posX, int posY, int typeID, bool includeStartPos = true );
		List<GameObject> GetConnectedItems( GameObject targetItem, bool includeStartPos = true );
		List<GameObject> GetConnectedItems( GridItem targetItem, bool includeStartPos = true );

		List<GameObject> GetDisconnectedFromPointItems( int targetX, int targetY );
		#endregion
	}
}