﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Puzzalo
{
	public enum GridGrowthDirection : int
	{
		DOWN = -1,
		UP = 1,
	}

	public enum GridType
	{
		Triangular,
		Rectangular,
		Hexagonal,
	}

	public class Field : IField
	{
		public float distance = 0f;
		public GridGrowthDirection growthDirection = GridGrowthDirection.DOWN;

		private List<GameObject> prefabList = new List<GameObject> ();
		private AGrid gridInstance;
		private GameObject holder;
		private System.Random random = new System.Random();

		private List<object> autoAddComponentList = new List<object>();

		public Field(GridType gridType = GridType.Rectangular)
		{
			SetGameObjectHolder ();
			SetGrid (gridType);
		}

		#region FIELD_SETTINGS

		public void SetOrigin (Vector3 origin)
		{
			holder.transform.position = origin;
		}
		
		public void SetOrigin (Transform origin)
		{
			holder.transform.position = origin.position;
		}

		public bool AddItemPrefab( GameObject itemPrefab )
		{
			foreach (var item in prefabList)
				if (item == itemPrefab)
					return false;

			prefabList.Add (itemPrefab);
			return true;
		}

		#endregion

		#region GRID_OPEARATIONS

		public GameObject CreateItem( int posX, int posY, int type = -1)
		{
			var offset = GetItemOffset( posX, posY);
			return CreateItem( posX, posY, offset, type);
		}
		public GameObject CreateItem (int posX, int posY, Vector3 position, int type)
		{
			type = (type > -1) ? type : random.Next(0, prefabList.Count);

			var itemGameObject = new GameObject ("Grid Item");
			itemGameObject.transform.position = position;
			itemGameObject.transform.parent = holder.transform;

			var itemScript = itemGameObject.AddComponent<GridItem> ();
			itemScript.posX = posX;
			itemScript.posY = posY;
			itemScript.field = this;
			itemScript.grid = gridInstance;

			foreach (var component in autoAddComponentList)
			{
				itemGameObject.AddComponent (System.Type.GetType(component.ToString()));
			}

			itemScript.type = type;

			gridInstance.RegisterItem (itemScript);
			
			return itemGameObject;
		}


		public void SetItemType(int type, GameObject item, AnimationClip changeAnimation = null)
		{
			var gridItem = item.GetComponent<GridItem>();
			if (item == null)
				throw new System.Exception("The items has no GridItem component");
			SetItemType (type, gridItem, changeAnimation);
		}

		public void SetItemType(int type, int itemPosX, int itemPosY, AnimationClip changeAnimation = null)
		{
			SetItemType (type, GetItemAt (itemPosX, itemPosY), changeAnimation);
		}

		public void SetItemType(int type, GridItem item, AnimationClip changeAnimation = null)
		{
			item.type = type;
		}

		public System.Collections.Generic.List<GameObject> FillGrid (int numX, int numY)
		{
			int[] allIDs = new int[prefabList.Count];
			
			for (int i = 0; i < prefabList.Count; i++)
				allIDs[i] = i;
			
			return FillGrid (numX, numY, allIDs);
		}

		public System.Collections.Generic.List<GameObject> FillGrid (int numX, int numY, int typeID)
		{
			int[] id = new int[1]{ typeID };
			return FillGrid (numX, numY, id);
		}

		public System.Collections.Generic.List<GameObject> FillGrid (int numX, int numY, int[] typeIDs)
		{
			List<GameObject> createdItems = new List<GameObject> ();
			
			for (int i = 0; i < numX; i++)
			{
				for (int j = 0; j < numY; j++)
				{
					var selectedType = typeIDs[random.Next(0, typeIDs.Length)];
					createdItems.Add( CreateItem( i, j, selectedType) );
				}
			}
			
			return createdItems;
		}

		public System.Collections.Generic.List<GameObject> AddLine (int num, int lineWidth)
		{
			// moving existing items
			gridInstance.MoveItems (0, num);
			
			var allItems = gridInstance.GetAllItems ();
			foreach (var item in allItems)
				item.transform.position = GetItemOffset( item.posX, item.posY);
			
			// creating new items
			List<GameObject> createdItems = new List<GameObject> ();
			
			for (int i = 0; i < lineWidth; i++)
				for (int j = 0; j < num; j++)
					createdItems.Add(CreateItem( i, j));
			
			return createdItems;
		}

		public void RemoveDisconnectedItems (AnimationClip clip = null)
		{
			var disconnectedItems = gridInstance.GetDisconnectedItems ();
			var gameObjectList = ConvertItemList2GameObjectList (disconnectedItems);
			DestroyItems (gameObjectList, clip);
		}

		public void DestroyItems (System.Collections.Generic.List<GameObject> items, AnimationClip clip = null)
		{
			foreach (var item in items)
			{
				if (item != null)
					DestroyItem( item, clip);
			}
		}

		public void RemoveLine (int posY, AnimationClip clip = null)
		{
			var itemsInLine = gridInstance.GetItemsByPosY (posY);
			var gameObjectList = ConvertItemList2GameObjectList (itemsInLine);
			DestroyItems (gameObjectList, clip);
		}
		#endregion

		#region GRID_MATCHING
		public GameObject GetItemAt (int posX, int posY)
		{
			var item = gridInstance.GetItemAt (posX, posY);
			if (item == null)
				return null;
			return item.gameObject;
		}


		public List<GameObject> GetAllItems()
		{
			return ConvertItemList2GameObjectList (gridInstance.GetAllItems ());
		}


		public System.Collections.Generic.List<GameObject> GetAllItemsOfType (int typeID)
		{
			var itemsOfType = gridInstance.GetItemsOfType (typeID);
			return ConvertItemList2GameObjectList (itemsOfType);
		}


		public System.Collections.Generic.List<GameObject> GetItemsInRad (int posX, int posY, int radius, bool includeStartPos = true)
		{
			var itemsInRad = gridInstance.GetItemsInRadius (posX, posY, radius);
			var gameObjects = ConvertItemList2GameObjectList (itemsInRad);
			if (includeStartPos == true)
				gameObjects.Add (GetItemAt (posX, posY));
			return gameObjects;
		}
		
		public System.Collections.Generic.List<GameObject> GetItemsInDirection (int startX, int startY, int dirX, int dirY, bool includeStartPos = true)
		{
			var itemsInDirection = gridInstance.GetItemsInDirection (startX, startY, dirX, dirY);
			var gameObjectList = ConvertItemList2GameObjectList (itemsInDirection);
			if (includeStartPos)
				gameObjectList.Add (GetItemAt (startX, startY));
			return gameObjectList;
		}

		/// <summary>
		/// Gets the items in direction of a certain type.
		/// </summary>
		/// <returns>The items in direction by type.</returns>
		/// <param name="startX">Start x.</param>
		/// <param name="startY">Start y.</param>
		/// <param name="dirX">Dir x.</param>
		/// <param name="dirY">Dir y.</param>
		/// <param name="type">Type.</param>
		/// <param name="includeStartPos">If set to <c>true</c> include start position.</param>
		/// <param name="stopOnMistype">If set to <c>true</c> stops when type is not met.</param>
		public List<GameObject> GetItemsInDirectionByType( int startX, int startY, int dirX, int dirY, int type, bool includeStartPos = true, bool stopOnMistype = false)
		{
			var itemsInDirection = gridInstance.GetItemsInDirectionByType (startX, startY, dirX, dirY, type, stopOnMistype);
			var gameObjectList = ConvertItemList2GameObjectList (itemsInDirection);
			if (includeStartPos)
				gameObjectList.Add (GetItemAt (startX, startY));
			return gameObjectList;
		}


		public System.Collections.Generic.List<GameObject> GetItemsInArea (int aX, int aY, int bX, int bY)
		{
			var itemsInArea = gridInstance.GetItemsInArea (aX, aY, bX, bY);
			return ConvertItemList2GameObjectList (itemsInArea);
		}


		public System.Collections.Generic.List<GameObject> GetItemsNeighbours (int posX, int posY)
		{
			var neighbours = gridInstance.GetItemsNeighbours (posX, posY);
			return ConvertItemList2GameObjectList (neighbours);
		}
		public System.Collections.Generic.List<GameObject> GetItemsNeighbours (GameObject targetItem)
		{
			var item = targetItem.GetComponent<GridItem> ();
			if (item == null)
				throw new System.Exception ("The provided GameObject does not have a GridItem component attached to it");
			return GetItemsNeighbours (item);
		}
		public System.Collections.Generic.List<GameObject> GetItemsNeighbours (GridItem targetItem)
		{
			return GetItemsNeighbours (targetItem.posX, targetItem.posY);
		}


		public System.Collections.Generic.List<GameObject> GetConnectedItems (int posX, int posY, int typeID, bool includeStartPos = true)
		{
			var connectedItems = gridInstance.GetConnectedItems (posX, posY, typeID);
			var gameObjectList = ConvertItemList2GameObjectList (connectedItems);
			
			if (includeStartPos == true)
				gameObjectList.Add( GetItemAt( posX, posY ));
			
			return gameObjectList;
		}
		public System.Collections.Generic.List<GameObject> GetConnectedItems (GameObject targetItem, bool includeStartPos = true)
		{
			var item = targetItem.GetComponent<GridItem> ();
			if (item == null)
				throw new System.Exception ("The provided GameObject does not have a GridItem component attached to it");
			return GetConnectedItems (item);
		}
		public System.Collections.Generic.List<GameObject> GetConnectedItems (GridItem targetItem, bool includeStartPos = true)
		{
			return GetConnectedItems (targetItem.posX, targetItem.posY, targetItem.type);
		}


		public List<GameObject> GetDisconnectedFromPointItems( int targetX, int targetY)
		{
			var connectedItems = GetConnectedItems (targetX, targetY, -1, true);
			
			var allItems = ConvertItemList2GameObjectList( gridInstance.GetAllItems () );
			
			foreach (var item in connectedItems)
				allItems.Remove(item);
			
			return allItems;
		}

		#endregion

		public Vector3 GetItemOffset( int posX, int posY )
		{
			var rawOffset = gridInstance.GetItemOffset (posX, posY);

			var offsetX = rawOffset.x + distance * posX;
			var offsetY = (rawOffset.y + distance * posY) * (int)growthDirection;

			Vector3 offset = new Vector3 (offsetX, offsetY, 0) + holder.transform.position;
			return offset;
		}

		#region PRIVATE METHODS

		void SetGameObjectHolder ()
		{
			holder = new GameObject ("Puzzlo Item Holder");
		}

		void SetGrid (GridType gridType)
		{
			switch (gridType)
			{
				case GridType.Rectangular:

					gridInstance = new RectGrid();
					break;

				case GridType.Hexagonal:

					gridInstance = new HexGrid();
					break;

				default: throw new System.Exception( "Grid type " + gridType.ToString() +" is not supported");
			}
		}

		List<GameObject> ConvertItemList2GameObjectList (List<GridItem> itemList)
		{
			var list = new List<GameObject> ();
			
			foreach (var item in itemList)
				if (item != null)
				list.Add(item.gameObject);
			
			return list;
		}

		void DestroyItem (GameObject item, AnimationClip clip)
		{
			var script = item.GetComponent<GridItem> ();

			if (script != null)
			{
				script.isDestroyed = true;
				gridInstance.UnregisterItem (script);
			}

			GameObject.Destroy (item);
		}
		#endregion

		#region INTERNAL METHODS

		internal GameObject GetItemPrefab(int typeID)
		{
			return GameObject.Instantiate (prefabList [typeID]) as GameObject;
		}

		#endregion

		#region COMPLEMEMTARY METHODS

		/// <summary>
		/// Adds the component to all items.
		/// </summary>
		/// <param name="excludeDuplicates">If set to <c>true</c> exclude duplicates.</param>
		/// <param name="autoAddToNewItems">If set to <c>true</c> auto add to new items by calling the AddAutoAddComponent method. The AddAutoAddComponent(true) will always be excluding duplicates.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void AddComponentToAllItems<T> (bool excludeDuplicates = true, bool autoAddToNewItems = true) where T:UnityEngine.Component
		{
			var allItems = gridInstance.GetAllItems ();
			foreach (var item in allItems)
			{
				var comp = item.gameObject.GetComponent<T> ();
				if (comp != null && excludeDuplicates == true)
					continue;
				item.gameObject.AddComponent<T> ();
			}

			if (autoAddToNewItems == true)
			AddAutoAddComponent<T> ();
		}


		/// <summary>
		/// Adds the auto-add component. Does not add Component to existing field items.
		/// </summary>
		/// <param name="excludeDuplicates">If set to <c>true</c> exclude duplicates.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void AddAutoAddComponent<T>(bool excludeDuplicates = true) where T:UnityEngine.Component
		{
			if (excludeDuplicates == true)
				foreach (var item in autoAddComponentList)
					if (item == typeof(T))
						return;

			autoAddComponentList.Add (typeof(T));
		}

		#endregion
	}
}